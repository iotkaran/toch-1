#include "Global.h"
#include "Function.h"


bool Function::Recive_Jason(String message)
{
	//link example jason 6 object 
	//https://arduinojson.org/v6/api/jsonobject/createnestedobject/
	//define data to object jason1
	DynamicJsonDocument Massage(2048);
	deserializeJson(Massage, message);
	String From = Massage["From"];
	String To = Massage["To"];
	String Info = Massage["Info"];
	DynamicJsonDocument jason_From(100);
	char from[100];
	From.toCharArray(from, 100);
	char to[100];
	To.toCharArray(to, 100);
	char info[350];
	Info.toCharArray(info, 350);
	deserializeJson(jason_From, from);
	DynamicJsonDocument jason_To(100);
	deserializeJson(jason_To, to);
	DynamicJsonDocument jason_Info(1024);
	deserializeJson(jason_Info, info);
	String Data = jason_Info["Data"];
	char data[300];
	Data.toCharArray(data, 300);
	DynamicJsonDocument jason_Data(512);
	deserializeJson(jason_Data, data);

	// Values 
	String type_from = jason_From["Type"];
	String Token_from = jason_From["Token"];
	if (Token_from == "null")
	{
		Serial.println("Protocol_Error1");
		return 0;
	}
	Token_From = Token_from;
	int rule_from = jason_From["Rule"];
	String type_to = jason_To["Type"];
	String Token_to = jason_To["Token"];
	int rule_to = jason_To["Rule"];
	String time = Massage["Time"];
	String type_info = jason_Info["Type"];
	String action = jason_Info["Action"];
	int command = jason_Info["Command"];
	if (type_from == "null" | type_to == "null" | Token_to == "null" | time == "null" | type_info == "null" | action == "null" | command == NULL)   // Check Protocol
	{
		Serial.println("Protocol_Error2");
		return 0;
	}
	Type_From = type_from;
	Rule_From = rule_from;
	Type_To = type_to;
	Token_To = Token_to;
	Rule_To = rule_to;
	Time = time;
	Type_Info = type_info;
	Action = action;
	Command = command;
	switch (Command)																							// Detect Command Type
	{
	case 1:
	{
		String wifi = jason_Data["wifi"];
		String pass = jason_Data["pass"];
		String user = jason_Data["user_register"];
		int rule = jason_Data["Rule"];
		String Token = jason_Data["Token"];
		if (wifi == "null" | pass == "null")//| user == "null" )									    // Check Protocol
		{
			Serial.println("Protocol_Error3");
			return 0;
		}
		Given_ssid = wifi;
		Given_pass = pass;
		Given_User = user;
		Given_Rule = rule;
		Given_Token = Token;
		break;
	}
	case 2: 
	{
		String status = jason_Data["Status"];
		if (status == "null")																				    // Check Protocol
		{
			Serial.println("Protocol_Error4");
			return 0;
		}
		Given_Status = status;
		Switch_Status_Array[0] = status;
		break;
	}
	case 3:
	{
		String wifi = jason_Data["wifi"];
		String pass = jason_Data["pass"];
		String user = jason_Data["user_register"];
		int rule = jason_Data["Rule"];
		if (wifi == "null" | pass == "null" | user == "null")										// Check Protocol
		{
			Serial.println("Protocol_Error5");
			return 0;
		}
		Given_ssid = wifi;
		Given_pass = pass;
		Given_User = user;
		Given_Rule = rule;
		break;
	}
	case 4:
	{
		Given_Factory_Reset = jason_Data["factory"];
		if (Given_Factory_Reset == NULL)
		{
			Serial.println("Protocol_Error6");
			return 0;
		}
		break;
	}
	case 5:
	{
		// Nothing
		break;
	}
	case 7:
	{
		String link_ota = jason_Data["link"];
		Link_OTA = link_ota;
		break;
	}
	case 15:
	{
		int Mode = jason_Data["Mode"];
		String wifi = jason_Data["wifi"];
		String pass = jason_Data["pass"];
		Given_Active_Mode = Mode;
		Given_ssid = wifi;
		Given_pass = pass;
		break;
	}
	default:
	{
		Serial.println("Command Not Available");
		return 0;
		break;
	}
	}
	return 1;
}


String Function::Send_Jason(String Type_From, String Token_From, int Rule_From, String Type_To, String Token_To, int Rule_To, String Time, String Type_Info, String Action, int Command, String Status)//, String Key = "", String Info_Key = "")
{
	//link example jason 6 object 
	//https://arduinojson.org/v6/api/jsonobject/createnestedobject/

	//object jason for send
	StaticJsonDocument<500> doc;
	JsonObject Message = doc.to<JsonObject>();
	JsonObject jason_From = Message.createNestedObject("From");
	JsonObject jason_To = Message.createNestedObject("To");
	JsonObject jason_Info = Message.createNestedObject("Info");
	JsonObject jason_Data = jason_Info.createNestedObject("Data");

	jason_From["Type"] = Type_From;
	jason_From["Token"] = Token_From;
	jason_From["Rule"] = Rule_From;
	jason_To["Type"] = Type_To;
	jason_To["Token"] = Token_To;
	jason_To["Rule"] = Rule_To;
	Message["Time"] = Time;
	jason_Info["Type"] = Type_Info;
	jason_Info["Action"] = Action;
	jason_Info["Command"] = Command;

	switch (Command)
	{
	case 1:
	{
		jason_Data["wifi"] = ssid;
		jason_Data["pass"] = pass;
		jason_Data["mac"] = getMacAddress();                                              // Get Mac Address
		jason_Data["user_register"] = User;
		jason_Data["Rule"] = Rule;
		jason_Data["Token"] = Switch_Token;
		break;
	}
	case 2:
	{
		jason_Data["Status"] = Status;
		break;
	}
	case 3:
	{
		jason_Data["wifi"] = ssid;
		jason_Data["pass"] = pass;
		jason_Data["mac"] = getMacAddress();                                             // Get Mac Address
		jason_Data["user_register"] = User;
		jason_Data["Rule"] = Rule;
		break;
	}
	case 4:
	{
		jason_Data["factory"] = Factory_Reset;
		break;
	}
	case 5:
	{
		// Nothing
		jason_Data["Test"] = WiFi.RSSI();
		break;
	}
	case 7:
	{
		jason_Data["link"] = Link_OTA;
		break;
	}
	case 15:
	{
		jason_Data["Mode"] = Given_Active_Mode;
		switch (Given_Active_Mode)
		{
		case 1:
		{
			jason_Data["wifi"] = Access_Point_ssid;
			jason_Data["pass"] = Access_Point_Pass;
			break;
		}
		case 2:
		{
			jason_Data["wifi"] = ssid;
			jason_Data["pass"] = pass;
			break;
		}
		}
		break;
	}

	}
	String finalJson = "";
	serializeJson(Message, finalJson);
	return (finalJson + "\n");
}


void Function::Write_Error(int code_error, String message)
{

	File file = SPIFFS.open("/ErrorDb.txt", "a");

	//StaticJsonDocument<300> root;
	//root[key] = Value;
	//serializeJson(root, file);
	String Time = " 1997-02-02 13:33:23";
	//char sbuf[300];
	//sprintf(sbuf,"Code %i :Message %s :Time %s\n",code_error , message, Time);
	file.print("reza");
	file.close();

}

String Function::Read_Error()
{

	File file = SPIFFS.open("/ErrorDb.txt", "r");

	while (file.available()) {
		Serial.write(file.read());
	}
	//StaticJsonDocument<500> doc;
	//deserializeJson(doc, file);
	//String  value = doc[key];
	String value;
	return value;
}

String Function::getMacAddress()
{
	WiFi.macAddress();
	return String(WiFi.macAddress());
}

void Function::Connect_Wifi()
{
	if (try_wifi++ >= 100)
	{
		char SSID[1000];
		char PASS[1000];
		ssid.toCharArray(SSID, 1000);
		pass.toCharArray(PASS, 1000);
		WiFi.mode(WIFI_STA);
		WiFi.begin(SSID, PASS);
		Serial.print("Waiting for WiFi... ");
		Serial.print("Connecting to ");
		Serial.println(ssid);
		Wifi_Connected = 0;
		try_wifi = 0;
	}
	delay(100);
	if (WiFi.status() != WL_CONNECTED)
	{
		Connection_Change = 1;
		Connection_Type = 1;
	}
	else
	{
		Wifi_Connected = 1;
		Serial.println("WiFi connected");
		Serial.println("IP address: ");
		Serial.println(WiFi.localIP());
		Connection_Type = 2;								// Connect To Server 
		try_wifi = 100;
	}

	//for (int Delay = 0, i = 1; WiFi.status() != WL_CONNECTED; Delay++)
	//{
	//	if (Delay == 10 * i && WiFi.status() != WL_CONNECTED)
	//	{
	//		i++;
	//		ssid.toCharArray(SSID, 1000);
	//		pass.toCharArray(PASS, 1000);
	//		WiFi.begin(SSID, PASS);					     // Reconnect To Wifi
	//	}
	//	digitalWrite(Wifi_LED_Pin, !digitalRead(Wifi_LED_Pin));
	//	delay(500);
	//}
	//Wifi_Connected = 1;
	//Serial.println("WiFi connected");
	//Serial.println("IP address: ");
	//Serial.println(WiFi.localIP());
	//Connection_Type = 2;								// Connect To Server 
	//delay(500);
}

void Function::Connect_Server()
{
	Serial.print("Connecting to : ");
	Serial.println(ip);
	char IP[1000];
	ip.toCharArray(IP, 1000);
	client.setTimeout(100);
	client.connect(IP, port.toInt());
	delay(20);
	if (!client.connected())
	{
		Server_Connected = 0;
		Serial.println("Not Connect To Server");
		Connection_Change = 1;
		Connection_Type = 2;							// Reconnect To Server
		Active_Mode = 0;
	}
	else
	{
		delay(500);
		Server_Connected = 1;
		Serial.println("Connected to Server");
		Connection_Change = 0;
		Connection_Type = 0;
		Active_Mode = 1;								// Active In Client Mode
		String Log_In = Send_Jason(Switch_Type, Switch_Token, 1, "S", Server_ID, 1, "2019-12-01 12:22:21", "Request", "Get", 5, String(""));
		Serial.print("Send To Server : ");
		Serial.println(Log_In);
		char log_in[1000];
		Log_In.toCharArray(log_in, 1000);
		client.print(log_in);
		Send_ping = 1;
	}
}

void Function::Command_Server_Switch()
{
	if (Type_To == Switch_Type && Token_To == Switch_Token)
	{
		switch (Command)
		{
		case 1:
		{
			if (Type_Info == "Request")
			{
				if (Action == "Get")
				{
					Serial.println("1-Request-Get");
					String Res = Send_Jason(Switch_Type, Switch_Token, 1, Type_From, Token_From, Rule_From, Time, "Response", Action, Command, String(""));
					Send_To_Server(Res);
				}
			}
			break;
		}
		case 2:
		{
			if (Type_Info == "Request")
			{
				if (Action == "Set")
				{
					Serial.println("2-Request-Set");
					Switch_Status = Given_Status;
					Switch_Status_Change = 1;
					String Res = Send_Jason(Switch_Type, Switch_Token, 1, Type_From, Token_From, Rule_From, Time, "Response", Action, Command, Switch_Status);
					Send_To_Server(Res);
				}
				else if (Action == "Get")
				{
					Switch_Status = Switch_Status_Array[0];
					Serial.println("2-Request-Get");
					String Res = Send_Jason(Switch_Type, Switch_Token, 1, Type_From, Token_From, Rule_From, Time, "Response", Action, Command, Switch_Status);
					Send_To_Server(Res);
				}
			}
			else if (Type_Info == "Response")
			{
				if (Action == "Get")
				{
					Serial.println("2-Response-Get");
					//Switch_Status = Given_Status;
					Switch_Status_Change = 1;
				}
				else if (Action == "Get-N")
				{
					Serial.println("2-Response-Get-N");
					//Switch_Status = "0-0";
					Switch_Status_Array[0] = "0";
					Switch_Status_Change = 1;
				}
			}
			break;
		}
		case 7:
		{
			if (Type_Info == "Request")
			{
				if (Action == "Set")
				{
					Serial.println("7-Request-Get");
					String Res = Send_Jason(Switch_Type, Switch_Token, 1, Type_From, Token_From, Rule_From, Time, "Response", Action, Command, "");
					Send_To_Server(Res);
					Start_OTA(Link_OTA);
				}
			}
			break;
		}
		case 15:
		{
			switch (Given_Active_Mode)
			{
			case 1:
			{
				Access_Point_ssid = Given_ssid;
				Access_Point_Pass = Given_pass;
				String Res = Send_Jason(Switch_Type, Switch_Token, 1, Type_From, Token_From, Rule_From, Time, "Response", Action, Command, "");
				Send_To_Server(Res);
				Connection_Change = 1;
				Connection_Type = 3;						// Start Access Point Mode 
				Active_Mode = 0;
				break;
			}
			case 2:
			{
				ssid = Given_ssid;
				pass = Given_pass;
				EEPROM_writeString(2, ssid);
				EEPROM_writeString(52, pass);
				Serial.print("SSID : ");
				Serial.println(ssid);
				Serial.print("PASS : ");
				Serial.println(pass);
				String Res = Send_Jason(Switch_Type, Switch_Token, 1, Type_From, Token_From, Rule_From, Time, "Response", Action, Command, "");
				Send_To_Server(Res);
				WiFi.disconnect(true);
				delay(100);
				Connection_Change = 1;
				Connection_Type = 1;						// Start Station Mode
				Active_Mode = 0;
				break;
			}
			}
			break;
		}
		case 4:
		{
			Registerd = 0;
			EEPROM_writeString(0, String(Registerd));
			ESP.restart();
			break;
		}
		case 8:
		{
			ESP.restart();
			break;
		}
		}
	}
}
void Function::Set_AccessPoint()
{
	Serial.println("Start Access Point");
	if (client.connected() | client)
	{
		client.stop();
	}
	if (WiFi.status() == WL_CONNECTED)
	{
		WiFi.disconnect(true);
	}
	String ap_ssid = Access_Point_ssid;			// Set SSID For Access Point
	String ap_pass = Access_Point_Pass;
	char AP_SSID[1000];
	char AP_PASS[1000];
	ap_ssid.toCharArray(AP_SSID, 1000);
	ap_pass.toCharArray(AP_PASS, 1000);
	WiFi.mode(WIFI_AP);
	delay(500);
	WiFi.softAP(AP_SSID, AP_PASS);					// Start Access Point
	IPAddress myIP = WiFi.softAPIP();
	Serial.println(myIP);
	delay(500);
	wifiServer.begin();									// Start Server
	delay(500);
	Access_Point_Mode = 1;
	Server_Connected = 0;
	Wifi_Connected = 0;
	Connection_Change = 0;
	Connection_Type = 0;
	Active_Mode = 2;									// Active In Access Point Mode
}

void Function::Command_Phone_Switch()
{
	if (Type_To == Switch_Type) //&& Token_To == Switch_Token)
	{
		//if (Registerd)
		//{

		//}
		//else
		//{
		//}
		ssid = Given_ssid;
		pass = Given_pass;
		User = Given_User;
		Rule = Given_Rule;
		Switch_Token = Given_Token;
		String Res = Send_Jason(Switch_Type, Switch_Token, 1, Type_From, Token_From, Rule_From, Time, "Response", Action, Command, "");
		Serial.println(Res);
		char res[1000];
		Res.toCharArray(res, 1000);
		client.print(res);
		delay(100);
		EEPROM_writeString(2, ssid);
		EEPROM_writeString(52, pass);
		EEPROM_writeString(102, Switch_Token);
		Serial.print("SSID : ");
		Serial.println(ssid);
		Serial.print("PASS : ");
		Serial.println(pass);
		Access_Point_Mode = 0;
		Registerd = 1;								// Save In EEPROM
		EEPROM_writeString(0, String(Registerd));
		WiFi.softAPdisconnect(true);				// Turn Off Access Point
		Connection_Change = 1;
		Connection_Type = 1;						// Connect To Wifi 
		Active_Mode = 0;
	}
}

void Function::Get_Set_Switch()
{

}

void Function::Switch_Manager()
{
	if (Switch_Status_Change)
	{
		switch (Switch_Status_Array[0].toInt())				// Switch For Touch 1 Status Change
		{
			case 0:
			{
				Serial.println("LED 1:OFF");
				digitalWrite(Relay1_Pin, LOW);						// Turn Off LED 1
				digitalWrite(LED1_Pin, HIGH);
				Switch_Status_Change = 0;
				break;
			}
			case 1:
			{
				Serial.println("LED 1:On");
				digitalWrite(Relay1_Pin, HIGH);					// Turn On LED 1
				digitalWrite(LED1_Pin, LOW);
				Switch_Status_Change = 0;
				break;
			}
		}
	}
	// Touch 1 
	//Touch1 = digitalRead(Touch1_Pin);
	//if (Touch1 && !Keep_Touch1)
	//{
	//	Serial.println("Touch1 Touched");
	//	Touch1_Timer = millis();
	//	Keep_Touch1 = 1;
	//}
	//else if (!Touch1 && Keep_Touch1 && !Change_Status_Touch1)
	//{
	//	if (millis() - Touch1_Timer >= 20)
	//	{
	//		Serial.println("Touch1 Keep For 20 Milli Seconds");
	//		digitalWrite(Relay1_Pin, !digitalRead(Relay1_Pin));
	//		digitalWrite(LED1_Pin, !digitalRead(Relay1_Pin));
	//		Switch_Status_Array[0] = String(digitalRead(Relay1_Pin));
	//		if (client.connected())
	//		{
	//			Switch_Status = Switch_Status_Array[0];
	//			String status = Send_Jason(Switch_Type, Switch_Token, 1, "S", Server_ID, 0, "2019-12-01 12:22:21", "Request", "Set", 2, Switch_Status);
	//			Send_To_Server(status);
	//		}
	//		Change_Status_Touch1 = 1;
	//		Last_Change_Touch1 = millis();
	//	}
	//}
	//else if (Touch1 && Keep_Touch1)//&& !Change_Status_Touch1 )
	//{
	//	if (millis() - Touch1_Timer >= 5000)
	//	{
	//		ESP.restart();
	//	}
	//	else if (!Touch1_3s && millis() - Touch1_Timer >= 3000)
	//	{
	//		Serial.println("Touch1 Keep For 3 Seconds");
	//		Connection_Change = 1;                          // Connection_Change Flag
	//		Connection_Type = 3;                        // Switch Start As Access Point
	//		Active_Mode = 0;
	//		Change_Status_Touch1 = 1;
	//		Last_Change_Touch1 = millis();
	//		Touch1_3s = 1;
	//	}
	//}
	//if (Change_Status_Touch1)
	//{
	//	if (Touch1_3s && millis() - Last_Change_Touch1 >= 5000)
	//	{
	//		Keep_Touch1 = 0;
	//		Touch1_3s = 0;
	//		Change_Status_Touch1 = 0;
	//	}
	//	else if (!Touch1_3s && millis() - Last_Change_Touch1 >= 100)
	//	{
	//		Keep_Touch1 = 0;
	//		Change_Status_Touch1 = 0;
	//	}
	//}





	// Touch 2






	Touch1 = digitalRead(Touch1_Pin);
	if (Touch1 && Touch1_State == 0)
	{
		Touch1_Timer = millis();
		Touch1_State = 1;
	}
	else if (!Touch1 && Touch1_State == 1)
	{
		if (millis() - Touch1_Timer >= 20)
		{
			Serial.println("Touch1 Keep For 20 Milli Seconds");
			digitalWrite(Relay1_Pin, !digitalRead(Relay1_Pin));
			digitalWrite(LED1_Pin, !digitalRead(Relay1_Pin));
			Switch_Status_Array[0] = String(digitalRead(Relay1_Pin));
			if (client.connected())
			{
				Switch_Status = Switch_Status_Array[0];
				String status = Send_Jason(Switch_Type, Switch_Token, 1, "S", Server_ID, 0, "2019-12-01 12:22:21", "Request", "Set", 2, Switch_Status);
				Send_To_Server(status);
			}
		}
		Touch1_State = 0;
	}
	else if (Touch1 && Touch1_State == 1)
	{
		if (millis() - Touch1_Timer >= 3000)
		{
			Serial.println("Touch1 Keep For 3 Seconds");
			Connection_Change = 1;                          // Connection_Change Flag
			Connection_Type = 3;                        // Switch Start As Access Point
			Active_Mode = 0;
			Touch1_State = 2;
		}
	}
	else if (Touch1 && Touch1_State == 2)
	{
		if (millis() - Touch1_Timer >= 7000)
		{
			Serial.println("Touch1 Keep For 7 Seconds");
			ESP.restart();
		}
	}
	else if (!Touch1 && Touch1_State == 2)
	{
		Touch1_State = 0;
	}



	//Touch1 = digitalRead(Touch1_Pin);
	//if (Touch1 && !Keep_Touch1)									// If Touch 1 , Touched For The First Time
	//{
	//	Serial.println("Touch1 Touched");
	//	Touch1_Timer = millis();								// Start Timer For Touch 1
	//	Keep_Touch1 = 1;
	//}
	//Touch2 = digitalRead(Touch2_Pin);
	//if (Touch2 && !Keep_Touch2)									// If Touch 2 , Touched For The First Time
	//{
	//	Serial.println("Touch2 Touched");
	//	Touch2_Timer = millis();								// Start Timer For Touch 2
	//	Touch2_3s = 0;
	//	Keep_Touch2 = 1;
	//}
	//Touch1 = digitalRead(Touch1_Pin);
	//if (Keep_Touch1 && !Touch1)									// If Touch 1 Release After A While
	//{
	//	if (millis() - Touch1_Timer >= 3000)					// If Touch 1 , Keep For 3 Seconds
	//	{
	//		Serial.println("Touch1 Keep For 3 Seconds");
	//	}
	//	else if(millis() - Touch1_Timer >= 20)					// If Touch 1 , Keep For 20 Milli Seconds
	//	{
	//		Serial.println("Touch1 Keep For 20 Milli Seconds");
	//		digitalWrite(LED1_Pin, !digitalRead(LED1_Pin));
	//	}
	//	Keep_Touch1 = 0;
	//}
	//Touch2 = digitalRead(Touch2_Pin);
	//if (Keep_Touch2 && !Touch2 && !Touch2_3s)									// If Touch 2 Release After A While
	//{
	//	if (millis() - Touch2_Timer >= 20)					// If Touch 2 , Keep For 20 Milli Seconds
	//	{
	//		Serial.println("Touch2 Keep For 20 Milli Seconds");
	//		digitalWrite(LED2_Pin, !digitalRead(LED2_Pin));
	//	}
	//	Keep_Touch2 = 0;
	//}
	//else if (Keep_Touch2 && Touch2 && !Touch2_3s)
	//{
	//	if (millis() - Touch2_Timer >= 3000)					// If Touch 2 , Keep For 3 Seconds
	//	{
	//		Serial.println("Touch2 Keep For 3 Seconds");
	//		Keep_Touch2 = 0;
	//		Touch2_3s = 1;
	//	}	
	//}
}

void Function::Send_To_Server(String Message)
{
	Serial.print("Send To Server : ");
	Serial.println(Message);
	char res[1000];
	Message.toCharArray(res, 1000);
	client.print(res);											// Send Message To Server
}

void Function::Wifi_LED()
{
	if (Wifi_Connected && Server_Connected && !Access_Point_Mode)
	{
		digitalWrite(Wifi_LED_Pin, HIGH);
	}
	else if (Wifi_Connected && !Server_Connected && !Access_Point_Mode)
	{
		if (millis() - Timer_LED >= 100)
		{
			digitalWrite(Wifi_LED_Pin, !digitalRead(Wifi_LED_Pin));
			Timer_LED = millis();
		}
	}
	else if (!Wifi_Connected && !Server_Connected && Access_Point_Mode)
	{
		if (millis() - Timer_LED >= 1000)
		{
			digitalWrite(Wifi_LED_Pin, !digitalRead(Wifi_LED_Pin));
			Timer_LED = millis();
		}
	}
	else if (!Wifi_Connected && !Server_Connected && !Access_Point_Mode)
	{
		if (millis() - Timer_LED >= 500)
		{
			digitalWrite(Wifi_LED_Pin, !digitalRead(Wifi_LED_Pin));
			delay(50);
			digitalWrite(Wifi_LED_Pin, !digitalRead(Wifi_LED_Pin));
			delay(50);
			digitalWrite(Wifi_LED_Pin, !digitalRead(Wifi_LED_Pin));
			delay(50);
			digitalWrite(Wifi_LED_Pin, !digitalRead(Wifi_LED_Pin));
			delay(50);
			Timer_LED = millis();
		}
	}
}
void Function::Start_OTA(String Link)
{
	WiFiClient client_OTA;
	ESPhttpUpdate.setLedPin(Wifi_LED_Pin, LOW);
	char link[1000];
	Link.toCharArray(link, 1000);
	t_httpUpdate_return ret = ESPhttpUpdate.update(client_OTA, link);
	switch (ret)
	{
	case HTTP_UPDATE_FAILED:
		Serial.printf("HTTP_UPDATE_FAILD Error (%d): %s\n", ESPhttpUpdate.getLastError(), ESPhttpUpdate.getLastErrorString().c_str());
		break;

	case HTTP_UPDATE_NO_UPDATES:
		Serial.println("HTTP_UPDATE_NO_UPDATES");
		break;

	case HTTP_UPDATE_OK:
		Serial.println("HTTP_UPDATE_OK");
		break;
	}
}
void Function::EEPROM_writeString(char add, String data)
{
	int _size = data.length();
	int i;
	for (i = 0; i < _size; i++)
	{
		EEPROM.write(add + i, data[i]);
	}
	EEPROM.write(add + _size, '\0');   //Add termination null character for String Data
	EEPROM.commit();
}


String Function::EEPROM_readString(char add)
{
	int i;
	char data[500]; //Max 100 Bytes
	int len = 0;
	unsigned char k;
	k = EEPROM.read(add);
	while (k != '\0' && len < 500)   //Read until null character
	{
		k = EEPROM.read(add + len);
		data[len] = k;
		len++;
	}
	data[len] = '\0';
	return String(data);
}
void Function::PingSetup()
{
	//#include <lwip/icmp.h> // needed for icmp packet definitions
	pinger.OnReceive([](const PingerResponse& response)
		{
			if (response.ReceivedResponse)
			{
				//Serial.printf(
				//	"Reply from %s: bytes=%d time=%lums TTL=%d\n",
				//	response.DestIPAddress.toString().c_str(),
				//	//response.EchoMessageSize - sizeof(struct icmp_echo_hdr),
				//	response.ResponseTime,
				//	response.TimeToLive);
			}
			else
			{
				//Serial.printf("Request timed out.\n");
			}

			// Return true to continue the ping sequence.
			// If current event returns false, the ping sequence is interrupted.
			return true;
		});

	pinger.OnEnd([](const PingerResponse& response)
		{
			// Evaluate lost packet percentage
			float loss = 100;
			if (response.TotalReceivedResponses > 0)
			{
				loss = (response.TotalSentRequests - response.TotalReceivedResponses) * 100 / response.TotalSentRequests;
			}
			if (loss == 100)
			{
				// Internet Not available
				Serial.println("Internet Not Available");
				if (!Connection_Change && !Connection_Type)
				{
					Server_Connected = 0;
					client.stop();
					Active_Mode = 0;
					Connection_Change = 1;
					Connection_Type = 2;                                 // Reconnect Server
				}
			}
			// Print packet trip data
			//Serial.printf(
			//	"Ping statistics for %s:\n",
			//	response.DestIPAddress.toString().c_str());
			//Serial.printf(
			//	"    Packets: Sent = %lu, Received = %lu, Lost = %lu (%.2f%% loss),\n",
			//	response.TotalSentRequests,
			//	response.TotalReceivedResponses,
			//	response.TotalSentRequests - response.TotalReceivedResponses,
			//	loss);

			//// Print time information
			//if (response.TotalReceivedResponses > 0)
			//{
			//	Serial.printf("Approximate round trip times in milli-seconds:\n");
			//	Serial.printf(
			//		"    Minimum = %lums, Maximum = %lums, Average = %.2fms\n",
			//		response.MinResponseTime,
			//		response.MaxResponseTime,
			//		response.AvgResponseTime);
			//}

			//// Print host data
			//Serial.printf("Destination host data:\n");
			//Serial.printf(
			//	"    IP address: %s\n",
			//	response.DestIPAddress.toString().c_str());
			//if (response.DestMacAddress != nullptr)
			//{
			//	Serial.printf(
			//		"    MAC address: " MACSTR "\n",
			//		MAC2STR(response.DestMacAddress->addr));
			//}
			//if (response.DestHostname != "")
			//{
			//	Serial.printf(
			//		"    DNS name: %s\n",
			//		response.DestHostname.c_str());
			//}
			if (Server_Connected)
			{
				Send_ping = 1;
			}
			return true;
		});
}
#pragma once
#include "Global.h"
class Function
{
public:
	void Format();
	void Clear_History();
	void Write_Error(int code_error, String message);
	String Read_Error();
	String getMacAddress();
	bool Recive_Jason(String message);
	String Send_Jason(String Type_From, String Token_From, int Rule_From, String Type_To, String Token_To, int Rule_To, String Time, String Type_Info, String Action, int Command, String Status);
	void Connect_Wifi();
	void Connect_Server();
	void Command_Server_Switch();
	void Command_Phone_Switch();
	void Set_AccessPoint();
	void Get_Set_Switch();
	void Switch_Manager();
	void Send_To_Server(String Message);
	void Wifi_LED();
	void Start_OTA(String Link);
	void EEPROM_writeString(char add, String data);
	String EEPROM_readString(char add);
	void PingSetup();
};

/*
 Name:		Wall_Switche.ino
 Created:	1/16/2020 1:21:11 PM
 Author:	Mohammad Mehdi Yaghooti
*/

//#include <TaskSchedulerSleepMethods.h>
//#include <TaskSchedulerDeclarations.h>
#include <ESP8266httpUpdate.h>
#include "TaskScheduler.h"
#include "Global.h"
#include "Function.h"
//void t1Callback();
//Scheduler runner;
//Task t1(100, TASK_FOREVER, &t1Callback);


void setup()
{
    Serial.begin(115200);                           // Start Serial
    EEPROM.begin(300);
    //runner.init();
    //Serial.println("Initialized scheduler");
    //runner.addTask(t1);
    //Serial.println("added t1");
    //delay(5000);
    //t1.enable();
    //Serial.println("Enabled t1");


    chipid = ESP.getChipId();                       // Get Esp ChipID

    Connection_Change = 1;                          // Connection_Change Flag
    Registerd = Func.EEPROM_readString(0).toInt();
    Serial.print("Registerd: ");
    Serial.println(Registerd);
    if (Registerd)                                // If Switch Start Registerd Before
    {
        ssid = Func.EEPROM_readString(2);
        pass = Func.EEPROM_readString(52);
        Switch_Token = Func.EEPROM_readString(102);
        Serial.print("ssid: ");
        Serial.println(ssid);
        Serial.print("pass: ");
        Serial.println(pass);
        Serial.println("Server Connection Mode");
        Connection_Type = 1;                        // Switch  Connect To Wifi
    }
    else
    {
        Serial.println("Access Point Mode");
        Connection_Type = 3;                        // Switch Start As Access Point
    }
    pinMode(LED1_Pin, OUTPUT);
    pinMode(Relay1_Pin, OUTPUT);                       // LED 1
    pinMode(Wifi_LED_Pin, OUTPUT);
    digitalWrite(Wifi_LED_Pin, LOW);
    digitalWrite(LED1_Pin, HIGH);
    digitalWrite(Relay1_Pin, LOW);
    pinMode(Touch1_Pin, INPUT);                      // Touch 1
    Func.PingSetup();
    ip.toCharArray(Server_IP, 50);
}

void loop()
{
    //runner.execute();
    if (Connection_Change)                          // If There Is Any Changes To Connections
    {
        switch (Connection_Type)                    // Connection_Change Type 
        {
        case 1:                                 // Connection_Type Is 1
        {
            Func.Connect_Wifi();                // Connect To Wifi
            break;
        }
        case 2:                                 // Connection_Type Is 2
        {
            Func.Connect_Server();              // Connect To Server
            break;
        }
        case 3:                                 // Connection_Type Is 3
        {
            Func.Set_AccessPoint();             // Start Access Point
            break;
        }
        }
    }
    if (Active_Mode)
    {
        switch (Active_Mode)
        {
        case 1:                                                         // Server Connection Mode
        {
            if (client.available())                                     // If There Is Any Message From Server
            {
                String Received_Data = client.readStringUntil('\n');    // Read Message
                Serial.print("Receive Server : ");
                Serial.println(Received_Data);
                if (Func.Recive_Jason(Received_Data))                   // If Protocol Correct
                {
                    Func.Command_Server_Switch();                       // Do The Command And Response
                }
            }
            if (millis() - Last_Check_Connection >= 10000)              // Check Every 10 Seconds
            {
                if (WiFi.status() != WL_CONNECTED)                      // Check Wifi Connection
                {
                    Serial.println("Wifi disconnected");
                    Wifi_Connected = 0;
                    Server_Connected = 0;
                    WiFi.disconnect(true);
                    Active_Mode = 0;
                    Connection_Change = 1;
                    Connection_Type = 1;                                // Reconnect Wifi
                }
                else if (!client.connected() | !client)                 // Check Server Connection
                {
                    Serial.println("Server disconnected");
                    Server_Connected = 0;
                    client.stop();
                    Active_Mode = 0;
                    Connection_Change = 1;
                    Connection_Type = 2;                                // Reconnect Server
                }
                //if (Send_ping)
                //{
                //    Serial.println("Send Ping");
                //    pinger.Ping(Server_IP, 3);
                //    Send_ping = 0;
                //}
                Last_Check_Connection = millis();
            }
            break;
        }
        case 2:                                                         // Access Point Mode
        {
            if (!client.connected() | !client)                          // If There Isn't Any Client  
            {
                client = wifiServer.available();                        // Listen For New Client
            }
            //else
            //{
            //    Serial.println("New Client");
            //}
            if (client.available())                                     // If There Is A Message From Client
            {
                String Received_Data = client.readStringUntil('\r');    // Read Message
                Serial.print("Receive Phone : ");
                Serial.println(Received_Data);
                if (Func.Recive_Jason(Received_Data))                   // If Protocol Correct
                {
                    Func.Command_Phone_Switch();                        // Do The Command And Response
                }
            }
            break;
        }
        }
    }
    Func.Switch_Manager();                            // Check Switch Status
    Func.Wifi_LED();
}
//void t1Callback()
//{
//    Func.Switch_Manager();                            // Check Switch Status
//    Func.Wifi_LED();
//}

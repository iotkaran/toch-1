#pragma once

#include "WString.h"
#include "FS.h"
#include "ArduinoJson.h"
#include <WiFiClient.h>
#include <ESP8266WebServer.h>
#include <ESP8266WiFi.h>        // Include the Wi-Fi library
//#include <TaskSchedulerSleepMethods.h>
//#include <TaskSchedulerDeclarations.h>
#include <ESP8266HTTPClient.h>
#include <ESP8266httpUpdate.h>
#include <EEPROM.h>
#include "EEPROM.h"
#include <PingerResponse.h>
#include <Pinger.h>
#include "Function.h"




//global value
extern String Switch_Token;
extern String Server_ID;
extern String Switch_Type;
extern String Switch_Status;
extern String Switch_Status_Array[1];
extern int chipid;
extern int Rule;
extern String User;
extern String ssid;
extern String pass;
extern String Access_Point_ssid;
extern String Access_Point_Pass;
extern String ip;
extern char Server_IP[50];
extern String port;
extern String Link_OTA; 
extern int Relay1_Pin;
extern int LED1_Pin;
extern int Wifi_LED_Pin;
extern int Touch1_Pin;



extern IPAddress Ap_local_IP;
extern IPAddress Ap_gateway;
extern IPAddress Ap_subnet;
extern IPAddress Ap_subnet;

extern int Connection_Type;
extern int Connection_Change;
extern String Type_From;
extern String Token_From;
extern int Rule_From;
extern String Type_To;
extern String Token_To;
extern int Rule_To;
extern String Time;
extern String Type_Info;
extern String Action;
extern int Command;
extern String Given_Status;;
extern int Given_Rule;
extern String Given_User;
extern String Given_ssid;
extern String Given_pass;
extern int Given_Active_Mode;
extern String Given_Token;
extern bool Factory_Reset;
extern bool Given_Factory_Reset;
extern unsigned long Last_Check_Connection;
extern unsigned long last_ping;
extern bool Registerd;
extern int Active_Mode;
extern WiFiClient client;
extern WiFiServer wifiServer;           // Access Point Listen On This Port
extern bool Switch_Status_Change;
extern bool Touch1;
extern int Touch1_State;
extern unsigned long Touch1_Timer;
extern unsigned long Timer_LED;
extern bool Wifi_Connected;
extern bool Server_Connected;
extern bool Access_Point_Mode;
extern int try_wifi;
extern bool Send_ping;
//extern Scheduler runner;
//extern Task t1;
extern Function Func;
extern Pinger pinger;
class Global
{

};
